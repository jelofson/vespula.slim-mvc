# README #

This is an MVC setup for quickly getting started with the Slim Framework version 3. It contains

* the Slim Framework,
* a template system via League/Plates,
* a localization library for displaying localized strings,
* a PSR-3 logger,
* an authentication system, and
* session management via Aura/Session

The full documentation is available at https://vespula.bitbucket.io/mvc/.

