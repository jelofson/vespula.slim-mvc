<?php

$bootstrap_info = '<div class="alert alert-info alert-dismissible" role="alert">' .
                  '     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' .
                  '     %s' .
                  '</div>';
$bootstrap_error = '<div class="alert alert-danger alert-dismissible" role="alert">' .
                   '     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' .
                   '     %s' .
                   '</div>';

$foundation_info = '<div data-alert class="alert-box info">' .
                    '    %s' .
                    '    <a href="#" class="close">&times</a>' .
                    '</div>';
$foundation_error = '<div data-alert class="alert-box alert">' .
                    '    %s' .
                    '    <a href="#" class="close">&times</a>' .
                    '</div>';

                    
$default_info = '<div style="padding: 5px; border: 1px solid #ccc; background-color: #A0D3E8; margin-bottom: 10px;">%s</div>';
$default_error = '<div style="padding: 5px; border: 1px solid #ccc; background-color: #fcc; margin-bottom: 10px;">%s</div>';

return [
    'bootstrap'=>[
        'info'=>$bootstrap_info,
        'error'=>$bootstrap_error
    ],
    'foundation'=>[
        'info'=>$foundation_info,
        'error'=>$foundation_error,
    ],
    'gcweb'=>[
        'info'=>$bootstrap_info,
        'error'=>$bootstrap_error,
    ],
    'gcwu'=>[
        'info'=>$bootstrap_info,
        'error'=>$bootstrap_error,
    ],
    'default'=>[
        'info'=>$default_info,
        'error'=>$default_error,
    ]
];


