<?php $this->layout('layouts::' . $theme) ?>

<?=$this->alerts($messages); ?>

<h2>Welcome</h2>

<p>Create your own project by running the command line tool and selecting 'p' for project.</p>

<pre>
$ ./vendor/bin/vespula-cli
</pre>

Read the <a href="https://vespula.bitbucket.io/mvc/">documenation on the CLI tools</a> for more information.



<p>After creating a project, modify your index.php file in the html folder to point to your project instead of the Welcome project.</p>

<p>If using Apache, make sure you update your .htaccess file.</p>
