<?php

namespace Welcome\Controllers;


class Index extends Controller {
	
	public function index()
	{
        $this->addErrorMessage('This could be an error message');
        $this->addInfoMessage('This could be an info message');
    

        $content = $this->view->render('index/index', [
            'messages'=>$this->messages
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
	}
}
