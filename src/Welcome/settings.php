<?php

// This file is ignored by git. You should use a settings-dist.php file in your VCS
// and this settings.php file can vary from environment to environment.

// Add your settings here, and they will be available via the container
// $settings = $container->get('settings');

// Please refer to the Slim Framework documentation for information on what these settings do.
return [
    'env'=>'development',
    'displayErrorDetails' => true,
    'logErrors' => true,
    'logErrorDetails' => true,
    'title'=>'Your Project',
    'theme'=>'default',
    'base_uri'=>'',
    'atlas'=>[
        'pdo'=>[
            'mysql:dbname=somedb;host=localhost;charset=utf8',
            'juser',
            '******'
        ],
        'namespace'=>'Project\\Models',
        'directory'=>'src/Project/Models'
    ],
    'form'=>[
        'default_classes'=>[
            'text'=>'form-control'
        ]
    ],
];
