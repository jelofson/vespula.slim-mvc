<?php
use Vespula\PlatesExtensions\Alerts;
use Vespula\Log\Log;
use Vespula\Log\Adapter\ErrorLog;

$container->add('settings', function () use ($settings) {
    return $settings;
});

$container->add('logAdapter', ErrorLog::class)->addArgument(ErrorLog::TYPE_PHP_LOG);
$container->add('log', Log::class)->addArgument('logAdapter');


$container->add('view', function () use ($container) {
    $settings = $container->get('settings');
    $theme = $settings['theme'];
    $title = $settings['title'];

    $views = dirname(__FILE__) . '/Views';
    $layouts = dirname(__FILE__) . '/Layouts';
    
    $view = new \League\Plates\Engine($views);
    $view->addFolder('layouts', $layouts);

    $view->addData([
        'title'=>$title,
        'theme'=>$theme
    ]);

    $alerts = new Alerts($container);
    
    $view->loadExtension($alerts);

    return $view;

});
