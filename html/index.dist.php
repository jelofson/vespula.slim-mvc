<?php
session_start();

require '../vendor/autoload.php';

use Slim\Factory\AppFactory;
use League\Container\Container;

// Include the settings file
$settings = require '../src/Welcome/settings.php';

// Get the container
$container = new Container;

AppFactory::setContainer($container);
$app = AppFactory::create();
$app->setBasePath($settings['base_uri']);

$app->addErrorMiddleware(
    $settings['displayErrorDetails'], 
    $settings['logErrors'], 
    $settings['logErrorDetails']
);

$routeParser = $app->getRouteCollector()->getRouteParser();
$container->add('router', $routeParser);

// Get dependencies
include '../src/Welcome/dependencies.php';

// include access control list rules
include '../src/Welcome/acl.php';

// Get middleware
include '../src/Welcome/middleware.php';

// include project routes
include '../src/Welcome/routes.php';

// Run app
$app->run();
